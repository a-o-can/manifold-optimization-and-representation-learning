rng(10);

% Define ddiag
ddiag = @(M) diag(diag(M));

% Define the cost function
cost = @(U, A) 0.5*(norm(U'*A*U - ddiag(U'*A*U),"fro")^2) ;

% calculation of the gradient
Gradient = @(U, A) 2*A*U*(U'*A*U - ddiag(U'*A*U));

% projection onto the tangent space (taken from coding assignment 6)
Proj = @(U, H) 0.5*U*(U'*H - H'*U);

%%
% Dimensionality
n = 7;

% initial matrices
A = sym_pd(n);
% A = diag(randperm(n));
U_init = orthogonal_group(n);

t_init = 1;
c1 = 10^-4;
c2 = 0.9;
beta = 0.5;
iter_max = 500;
iter_ls_max = 200;

U = U_init;

diff = 10;
stopping_error = 1e-15;

while diff > stopping_error
    t = t_init;
    % calculate cost
    f = cost(U, A);
    % calculate gradient
    G = Gradient(U, A);
    % project on to tangent space
    G = Proj(U, G);
    % choose search direction as the negative of the projected gradient
    H = -G;
    
    % linesearch
    f_ls = f;
    H_pt = H;
    G_update = G;
    iter_ls = 1;
    while iter_ls < iter_ls_max
        % get next U by retraction
        U_next = Retraction(U, H, t);
        % calculate loss of next U
        f_ls = cost(U_next, A);
        % calculate gradient of next U
        G_update = Gradient(U_next, A);
        % project on to the tangent space of the next U
        G_update = Proj(G_update, U_next);
        % transport the current search direction to the tangent space of
        % the next U
        H_pt = Vt(U,H,H);
        % Wolfe conditions
        if ((f_ls < f + c1*t*trace(G'*H)) && (trace(G_update'*H_pt) >= c2*trace(G'*H)))
            break;
        end
        t = beta*t;
        iter_ls = iter_ls+1;
    end
    diff = norm(U-U_next, "fro");
    U = U_next;
    % step_values(i) = t;
end
disp("We can check, whether our optimazation is correct. From taks 1, " + ...
    " we found that, the U^{optimum} should contain the eigenvectors of A in its" + ...
    " columns. Also, the multiplication of A with the transpose of U^{optimum}" + ...
    " from left and U^{optimum} from right should give back the eigenvalues of A.")
[V, D] = eig(A);
disp("Eigenvectors: ");
V
disp("U^{optimum}: ")
U_next
disp("As we see, the columns of U^{optimum} correspond to the permutation of" + ...
    " eigenvectors of A.")
disp("U^{optimum}' * A * U^{optimum}: ")
U_next' * A * U_next
disp("Eigenvalues: ");
D
disp("We see here, that the matrix is diagonal and contains the eigenvalues" + ...
    "of A in the same permuted order as they appear in the case of eigenvectors.")
%% Function definitions

% Retraction is used instead of the geodesic
function Q = Retraction(U, H, t)
    [Q, ~] = qr(U+(t*H));
end

% Define skew
function M_skew = skew(M)
    M_skew = (M - M.') / 2;
end

% Define vector transport
function H_pt = Vt(U, psi, delta)
    [Q, R] = qr(U + psi);
    H_pt = Q*skew(Q' * delta/R); % delta/R mimics delta*inv(R)
end

% Symmetric positive definit matrix
function M = sym_pd(n)
    A = randn(n, n);
    M = A* A';
end

% Function to create a matrix belonging to O(n)
function M = orthogonal_group(n)
    X_init = randn(n);
    [M,~] = qr(X_init);
end