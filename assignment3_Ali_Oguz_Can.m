% Newton like for native Rayleigh quotient on the unit sphere
% Copyright: Hao Shen, Ali Oğuz Can @ 2023
clc

m = 5;
Q = randn(m);
[Q R] = qr(Q);
A = Q' * diag([1:m]) * Q;

Q = randn(m);
[Q R] = qr(Q);
B = Q' * diag([1:m]) * Q;

X = randn(m);
[X R] = qr(X);

x = X(:,1);
xlist = x;

for itr = 1:100
    z = x;
    
    grad_coeff = 2/((x'*B*x)^2);
    grad = grad_coeff.*(X' * (A*x*x'*B*x - B*x*x'*A*x));
    grad = grad(2:m);

    hess_coeff2 = grad_coeff * (4/(x'*B*x));
    hess1 = grad_coeff .* ((x'*B*x)*X'*A*X - (x'*A*x)*X'*B*X);
    hess2 = hess_coeff2 .* (((x'*A*x)*X'*B*x - (x'*B*x)*X'*A*x)*x'*B*X );
    hess = hess1 + hess2;
    hess = hess(2:m,2:m);

    newd = - inv(hess) * grad;
    [Q R] = uqr(eye(m) + [0 -newd'; newd zeros(m-1)]);

    X = X * Q;
    x = X(:,1);
    
    fval = (x' * A * x)/(x' * B * x);
    xlist = [xlist x];

    if norm(x-z) < 1e-15
        x
        break
    end
end

nlist = size(xlist, 2);
elist = [];
xend = xlist(:,nlist);
for idx = 1:(nlist-1)
    elist = [elist norm(xlist(:,idx)-xend)];
end


figure(1)
semilogy(elist,'o-')
xlabel('iterate (k)')
ylabel('||X_{k} - X^{*}||_{F}')

disp("We can also do a sanity check. We know by the first sub question" + ...
    " that the algorithm should converge to the eigenvectors of the matrix" + ...
    " pencil (A,B). In this run of" + ...
    " the algorithm, we have the following eigenvalues and the eigenvectors:")
M = B\A;
[V, D] = eig(M)
disp("We can see that our algorithm has converged to one of the eigenvalues.")

function [Q, R] = uqr(M)
    [Q, R] = qr(M);
    D = diag(sign(diag(R)));
    Q = Q*D; 
    R = D*R;
end